package org.droidtr.dbtest;

import android.graphics.*;
import android.content.*;
import java.io.*;

public class bmpdb{

	private String txtbase="";
	public int MODE_BITMAP=0;
	public int MODE_TEXT=1;
 	private String[] getArray(){
		return getArray(txtbase);
	}
 	private String[] getArray(Bitmap b){
 	 	String raw = readBitmap(b);
 	 	String[] array = raw.split(";");
 	 	return array;
 	}
	private String[] getArray(String data){
		String[] array = data.split(";");
 	 	return array;
	}
 	private Bitmap putArray(String[] array){
 	 	return writeBitmap(putArrayText(array));
 	}
	private String putArrayText(String[] array){
		String raw = ";";
 	 	for(int i=0;i<array.length;i++){
 	 	 	raw=raw+";"+array[i];
 	 	}
		return raw;
	}
 	public Integer getInt(String str,Integer def){
		return getInt(txtbase,str,def);
	}
 	public Integer getInt(Bitmap b, String str,Integer def){
 	 	try {
 	 	 	return Integer.parseInt(getString(b, str, Integer.toString(def)));
 	 	}catch (Exception e){
 	 	 	return def;
 	 	}
 	}
	public Integer getInt(String data, String str,Integer def){
 	 	try {
 	 	 	return Integer.parseInt(getString(data, str, Integer.toString(def)));
 	 	}catch (Exception e){
 	 	 	return def;
 	 	}
 	}
 	public String getString(String str,String def){
		return getString(txtbase,str,def);
	}
 	public String getString(Bitmap b, String str,String def){
 	 	String[] array = getArray(b);
 	 	String value=def;
 	 	for(int i=0;i<array.length;i++){
 	 	 	if(array[i].split(",")[0].equals(EncodeChar(str))){
 	 	 	 	value = DecodeChar(array[i].split(",")[1]);
 	 	 	 	break;
 	 	 	}
 	 	}
 	 	return value;
 	}
	public String getString(String data,String str,String def){
		String[] array = getArray(data);
 	 	String value=def;
 	 	for(int i=0;i<array.length;i++){
 	 	 	if(array[i].split(",")[0].equals(EncodeChar(str))){
 	 	 	 	value = DecodeChar(array[i].split(",")[1]);
 	 	 	 	break;
 	 	 	}
 	 	}
 	 	return value;
	}
 	public Float getFloat(String str,Float def){
		return getFloat(txtbase,str,def);
	}
 	public Float getFloat(Bitmap b, String str,Float def){
 	 	try {
 	 	 	return Float.parseFloat(getString(b, str, Float.toString(def)));
 	 	}catch (Exception e){
 	 	 	return def;
 	 	}
 	}
	public Float getFloat(String data, String str,Float def){
 	 	try {
 	 	 	return Float.parseFloat(getString(data, str, Float.toString(def)));
 	 	}catch (Exception e){
 	 	 	return def;
 	 	}
 	}
 	public Double getDouble(String str,Double def){
		return getDouble(txtbase,str,def);
	}
 	public Double getDouble(Bitmap b, String str,Double def){
 	 	try {
 	 	 	return Double.parseDouble(getString(b, str, Double.toString(def)));
 	 	}catch (Exception e){
 	 	 	return def;
 	 	}
 	}
	public Double getDouble(String data, String str,Double def){
 	 	try {
 	 	 	return Double.parseDouble(getString(data, str, Double.toString(def)));
 	 	}catch (Exception e){
 	 	 	return def;
 	 	}
 	}
 	public Short getShort(String str,Short def){
		return getShort(txtbase,str,def);	
	}
 	public Short getShort(Bitmap b, String str,Short def){
 	 	try {
 	 	 	return Short.parseShort(getString(b, str, Short.toString(def)));
 	 	}catch (Exception e){
 	 	 	return def;
 	 	}
 	}
	public Short getShort(String data, String str,Short def){
 	 	try {
 	 	 	return Short.parseShort(getString(data, str, Short.toString(def)));
 	 	}catch (Exception e){
 	 	 	return def;
 	 	}
 	}
 	public Long getLong(String str,Long def){
		return getLong(txtbase,str,def);
	}
 	public Long getLong(Bitmap b, String str,Long def){
 	 	try {
 	 	 	return Long.parseLong(getString(b, str, Long.toString(def)));
 	 	}catch (Exception e){
 	 	 	return def;
 	 	}
 	}
	public Long getLong(String data, String str,Long def){
 	 	try {
 	 	 	return Long.parseLong(getString(data, str, Long.toString(def)));
 	 	}catch (Exception e){
 	 	 	return def;
 	 	}
 	}
 	public Boolean getBoolean(String str,Boolean def){
		return getBoolean(txtbase,str,def);
	}
 	public Boolean getBoolean(Bitmap b, String str,Boolean def){
 	 	int i =1;
 	 	if(def){
 	 	 	i=0;
 	 	}
 	 	return (getInt(b,str,i)==0);
 	}
	public Boolean getBoolean(String data, String str,Boolean def){
 	 	int i =1;
 	 	if(def){
 	 	 	i=0;
 	 	}
 	 	return (getInt(data,str,i)==0);
 	}
	public Character getCharacter(String str,Character def){
		return getCharacter(txtbase,str,def);
	}
	public Character getCharacter(Bitmap b, String str,Character def){
 	 	try {
 	 	 	return getString(b, str, Character.toString(def)).toCharArray()[0];
 	 	}catch (Exception e){
 	 	 	return def;
 	 	}
 	}
	public Character getCharacter(String data, String str,Character def){
 	 	try {
 	 	 	return getString(data, str, Character.toString(def)).toCharArray()[0];
 	 	}catch (Exception e){
 	 	 	return def;
 	 	}
 	}
	public Bitmap getBitmap(String str,Bitmap def){
		return getBitmap(txtbase,str,def);
	}
	public Bitmap getBitmap(Bitmap b, String str,Bitmap def){
 	 	try {
 	 	 	return writeBitmap(getString(b, str, readBitmap(def)));
 	 	}catch (Exception e){
 	 	 	return def;
 	 	}
 	}
	public Bitmap getBitmap(String data, String str,Bitmap def){
 	 	try {
 	 	 	return writeBitmap(getString(data, str, readBitmap(def)));
 	 	}catch (Exception e){
 	 	 	return def;
 	 	}
 	}
 	public String readBitmap() {
		return txtbase;
	}
 	public String readBitmap(Bitmap b) {
 	 	String s = "";
 	 	for (int i = 0; i <  b.getHeight(); i++) {
 	 	 	for (int j = 0; j < b.getWidth(); j++) {
 	 	 	 	int c = b.getPixel(j, i);
 	 	 	 	int num = toInteger(c);
 	 	 	 	if(num !=0) {
 	 	 	 	 	s=s+(char)num;
 	 	 	 	}
 	 	 	}
 	 	}
 	 	s=s.replaceAll(";;",";");
 	 	return s;
 	}
 	private int toInteger(Integer c){
 	 	int yuzler = Color.red(c)/4;
 	 	int onlar = Color.green(c)/4;
 	 	int birler = Color.blue(c)/4;
 	 	return yuzler*4096+onlar*64+birler;
 	}
 	private int toColor(Integer num){
 	 	int birler = num % (64);
 	 	int onlar =(num -birler)/64;
 	 	int yuzler =num -onlar*64-birler;
 	 	return Color.rgb(yuzler*4,onlar*4,birler*4);
 	}
	public Bitmap writeBitmap(){
		return writeBitmap(txtbase);
	}
 	public Bitmap writeBitmap(String s) {
 	 	int distance=s.length()^(1/2)+1;
		char[] sarray = s.toCharArray();
		int k=0;
 	 	Bitmap b = Bitmap.createBitmap(distance, distance, Bitmap.Config.ARGB_8888);
 	 	for (int i = 0; i < distance; i++) {
 	 	 	for (int j = 0; j < distance; j++) {
 	 	 	 	if((j * distance) + i < s.length()) {
 	 	 	 	 	if(i<b.getHeight() && j <b.getWidth()) {
 	 	 	 	 	 	int num=sarray[k];
						k++;
 	 	 	 	 	 	b.setPixel(j, i, toColor(num));
 	 	 	 	 	}
 	 	 	 	}
 	 	 	}
 	 	}
 	 	return b;
 	}
	public void putString(String str, String val){
		txtbase=putString(txtbase,str,val);	
	}
 	public Bitmap putString(Bitmap b, String str, String val){
 	 	String[] array = getArray(b);
 	 	String s= "";
 	 	for(int i=0;i<array.length;i++){
 	 	 	if(array[i]!="") {
 	 	 	 	if (!(DecodeChar(array[i].split(",")[0]).equals(str))) {
 	 	 	 	 	s = s +";"+ array[i] ;
 	 	 	 	}
 	 	 	}
 	 	}
 	 	if(val !=null) {
 	 	 	s = s + ";" + EncodeChar(str) + "," + EncodeChar(val);
 	 	}
 	 	return writeBitmap(s);
 	}
	public String putString (String data,String str,String val){
		String[] array = getArray(data);
 	 	String s= "";
 	 	for(int i=0;i<array.length;i++){
 	 	 	if(array[i]!="") {
 	 	 	 	if (!(DecodeChar(array[i].split(",")[0]).equals(str))) {
 	 	 	 	 	s = s +";"+ array[i] ;
 	 	 	 	}
 	 	 	}
 	 	}
 	 	if(val !=null) {
 	 	 	s = s + ";" + EncodeChar(str) + "," + EncodeChar(val);
 	 	}
 	 	return s;
	}
	public void putInt(String str,Integer val){
		putString(str,Integer.toString(val));
	}
 	public Bitmap putInt(Bitmap b,String str,Integer val){
 	 	return putString(b,str,Integer.toString(val));
 	}
	public String putInt(String data,String str,Integer val){
 	 	return putString(data,str,Integer.toString(val));
 	}

 	public Bitmap putFloat(Bitmap b,String str,Float val){
 	 	return putString(b,str,Float.toString(val));
 	}
	public String putFloat(String data,String str,Float val){
 	 	return putString(data,str,Float.toString(val));
 	}
	public void putFloat(String str,Float val){
 	 	putString(str,Float.toString(val));
 	}

 	public Bitmap putDouble(Bitmap b,String str,Double val){
 	 	return putString(b,str,Double.toString(val));
 	}
	public String putDouble(String data,String str,Double val){
 	 	return putString(data,str,Double.toString(val));
 	}
	public void putDouble(String str,Double val){
 	 	putString(str,Double.toString(val));
 	}

 	public Bitmap putCharacter(Bitmap b,String str,Character val){
 	 	return putString(b,str,val.toString());
 	}
	public String putCharacter(String data,String str,Character val){
 	 	return putString(data,str,val.toString());
 	}
	public void putCharacter(String str,Character val){
 	 	putString(str,val.toString());
 	}

 	public Bitmap putBoolean(Bitmap b,String str,Boolean val){
 	 	if(val){
 	 	 	return putString(b, str, "0");
 	 	}else{
 	 	 	return putString(b, str, "1");
 	 	}
 	}
	public String putBoolean(String data,String str,Boolean val){
 	 	if(val){
 	 	 	return putString(data, str, "0");
 	 	}else{
 	 	 	return putString(data, str, "1");
 	 	}
 	}
	public void putBoolean(String str,Boolean val){
 	 	if(val){
 	 		putString(str, "0");
 	 	}else{
 	 	 	putString(str, "1");
 	 	}
 	}

 	public Bitmap putBitmap(Bitmap b,String str,Bitmap val){
 	 	return putString(b,str,readBitmap(val));
 	}
	public String putBitmap(String data,String str,Bitmap val){
 	 	return putString(data,str,readBitmap(val));
 	}
	public void putBitmap(String str,Bitmap val){
 	 	putString(str,readBitmap(val));
 	}

 	public Bitmap putLong(Bitmap b,String str,Long l){
 	 	return putString(b,str,Long.toString(l));
 	}
	public String putLong(String data,String str,Long l){
 	 	return putString(data,str,Long.toString(l));
 	}
	public void putLong(String str,Long l){
 	 	putString(str,Long.toString(l));
 	}

 	public Bitmap putShort(Bitmap b,String str,Short l){
 	 	return putString(b,str,Short.toString(l));
 	}
	public String putShort(String data,String str,Short l){
 	 	return putString(data,str,Short.toString(l));
 	}
	public void putShort(String str,Short l){
 	 	putString(str,Short.toString(l));
 	}

 	private String DecodeChar(String str){
 	 	str=str.replace("&#",",");
 	 	str= str.replace("&@",";");
 	 	str=str.replace("&?","&");
 	 	return str;
 	}
 	private String EncodeChar(String str){
 	 	str=str.replace("&","&?");
 	 	str= str.replace(";","&@");
 	 	str=str.replace(",","&#");
 	 	return str;
 	}

	public boolean saveBitmap(String path){
		return saveBitmap(writeBitmap(txtbase),path);
	}
	public boolean saveBitmap(Bitmap b,String path){
		try
		{
			FileOutputStream fos = new FileOutputStream(path);
			b.compress(Bitmap.CompressFormat.PNG, 100, fos);
		return true;
		}catch (Exception e)
		{
			return false;
		}
	}
	public void loadBitmap(String path){
		txtbase=readBitmap(openBitmap(path));
	}
	public Bitmap openBitmap(String path){
		return BitmapFactory.decodeFile(path);
	}
	public void clear(){
		txtbase="";	
	}
}